composer: ## Install Composer dependencies
	docker-compose -f docker/docker-compose.yml exec app composer install

up:
	docker-compose -f docker/docker-compose.yml up -d --force-recreate

down:
	docker-compose -f docker/docker-compose.yml down

logs:
	docker-compose -f docker/docker-compose.yml logs --tail="all"

shell:
	docker-compose -f docker/docker-compose.yml exec app bash

migrate: ## Run migrations
	docker-compose -f docker/docker-compose.yml exec app bin/console doctrine:migrations:status
	docker-compose -f docker/docker-compose.yml exec app bin/console doctrine:migrations:migrate

test: ## Run PHPUnit tests
	docker-compose -f docker/docker-compose.yml exec app php bin/phpunit

phpstan: ## Run PHPStan
	docker-compose -f docker/docker-compose.yml exec app vendor/bin/phpstan analyze src
