Exchange rates notifiaction service
==============

### How to quickly start and test the project

On first run:

Copy and modify app/.env.dist to app/.env

Copy app/phpunit.xml.dist to app/phpunit.xml

```bash
$ make up
```

Next one, login into newly created container by

```bash
$ make shell
```

Load development fixtures with symfony command:
```bash
$ bin/console app:fixtures:load currency.yml
```