#!/bin/sh

rm -rf var/cache/*

composer install --dev

/var/www/app/bin/console doctrine:migrations:migrate --env=dev --no-interaction
/var/www/app/bin/console doctrine:schema:create --env=test

echo "Done"

exec "$@"
