<?php
declare(strict_types=1);

namespace App\Event;

use App\Entity\Client;
use Symfony\Contracts\EventDispatcher\Event;

class ClientConfirmedRegistrationEvent extends Event
{
    public const NAME = 'app.client.confirmed_registration';

    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getClient(): Client
    {
        return $this->client;
    }
}