<?php
declare(strict_types=1);

namespace App\Event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Twig\Environment;

class ClientSubscriber implements EventSubscriberInterface
{
    private \Swift_Mailer $mailer;

    private Environment $twig;

    private string $appUrl;

    public function __construct(\Swift_Mailer $mailer, Environment $twig, string $appUrl)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->appUrl = $appUrl;
    }

    public static function getSubscribedEvents()
    {
        return [
            ClientRegisteredEvent::NAME => 'onClientRegister',
            ClientConfirmedRegistrationEvent::NAME => 'onClientConfirmRegistration',
        ];
    }

    public function onClientRegister(ClientRegisteredEvent $event)
    {
        $client = $event->getClient();
        $link = $this->appUrl . '/confirm/' . $client->getConfirmationToken();
        $messageBody = $this->twig->render('emails/registration_confirmation.html.twig', ['link' => $link]);
        $message = (new \Swift_Message('Please confirm registration'))
            ->setFrom('mailer@localhost')
            ->setTo($client->getEmail())
            ->setBody($messageBody, 'text/html')
        ;

        $this->mailer->send($message);
    }
    
    public function onClientConfirmRegistration(ClientConfirmedRegistrationEvent $event)
    {
        $client = $event->getClient();
        $messageBody = $this->twig->render('emails/registration_confirmed.html.twig', ['apiToken' => $client->getApiToken()]);
        $message = (new \Swift_Message('Registration finished!'))
            ->setFrom('mailer@localhost')
            ->setTo($client->getEmail())
            ->setBody($messageBody, 'text/html')
        ;

        $this->mailer->send($message);
    }
}