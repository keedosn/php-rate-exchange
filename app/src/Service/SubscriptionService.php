<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Client;
use App\Entity\ClientSubscription;
use App\Entity\Currency;
use App\Exception\SubscriptionNotFoundException;
use Doctrine\Persistence\ObjectManager;

class SubscriptionService
{
    private ObjectManager $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    public function getSubscriptionsForClient(Client $client)
    {
        return $this->em->getRepository(ClientSubscription::class)->findByClient($client);
    }

    public function addSubscription(Client $client, Currency $currency, float $lowRateAlert, float $highRateAlert): ClientSubscription
    {
        $where = ['currency' => $currency->getId(), 'client' => $client];
        $subscription = $this->em->getRepository(ClientSubscription::class)->findOneBy($where);
        if (!$subscription) {
            $subscription = new ClientSubscription();
            $subscription->setClient($client);
            $subscription->setCurrency($currency);
        }

        $subscription->setLowRateAlert($lowRateAlert);
        $subscription->setHighRateAlert($highRateAlert);

        $this->em->persist($subscription);

        return $subscription;
    }

    public function removeSubscription(Client $client, Currency $currency, string $unsubscribeToken): bool
    {
        $where = ['currency' => $currency->getId(), 'unsubscribeToken' => $unsubscribeToken];
        $subscription = $this->em->getRepository(ClientSubscription::class)->findOneBy($where);
        if (!$subscription) {
            throw new SubscriptionNotFoundException();
        }

        $this->em->remove($subscription);
        $this->em->flush();

        return true;
    }
}