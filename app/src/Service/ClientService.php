<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Client;
use Symfony\Component\Mailer\MailerInterface;

class ClientService
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendRegistrationEmail(Client $client): bool
    {
        $body = 'Welcome. You receiving this email because you registered at exchange rates notification service.<br /><br />';
        $body .= 'Click this <a href="#">link</a> to confirm and finish registration.';

        $message = (new \Swift_Message('Subscription email'))
            ->setFrom('mailer@localhost')
            ->setTo($client->getEmail())
            ->setBody($body, 'text/html')
        ;

        return $this->mailer->send($message, $errors);
    }
}
