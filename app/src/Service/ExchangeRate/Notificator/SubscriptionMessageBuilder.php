<?php
declare(strict_types=1);

namespace App\Service\ExchangeRate\Notificator;

use App\Entity\Client;
use App\Entity\ClientSubscription;
use App\Service\SubscriptionService;
use Twig\Environment;

class SubscriptionMessageBuilder
{
    private array $data;

    private Client $client;

    private \DateTime $date;

    private SubscriptionService $subService;

    private Environment $twig;

    private string $appUrl;

    public function __construct(SubscriptionService $subService, Environment $twig, string $appUrl)
    {
        $this->subService = $subService;
        $this->appUrl = $appUrl;
        $this->twig = $twig;
    }

    public function prepare(array $data, Client $client, \DateTime $date): SubscriptionMessageBuilder
    {
        $this->data = $data;
        $this->client = $client;
        $this->date = $date;

        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function getMessageBody(): string
    {
        return $this->twig->render('emails/exchange_rates_notification.html.twig', [
            'items' => $this->getData(),
            'client' => $this->getClient(),
            'date' => $this->getDate(),
            'link' => $this->buildUnsubscribeLink()
        ]);
    }

    private function buildUnsubscribeLink(): string
    {
        $link = $this->appUrl . sprintf('/unsubscribe?', time());
        
        /** @var ClientSubscription $subscription */
        foreach ($this->subService->getSubscriptionsForClient($this->client) as $subscription) {
            $link .= sprintf('&t[]=%s', $subscription->getUnsubscribeToken());
        }

        return $link;
    }
}