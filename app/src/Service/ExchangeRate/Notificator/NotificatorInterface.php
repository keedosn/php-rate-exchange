<?php
declare(strict_types=1);

namespace App\Service\ExchangeRate\Notificator;

use App\Entity\Client;

interface NotificatorInterface
{
    public function sendNotification(array $subscriptionData, Client $client): int;
}