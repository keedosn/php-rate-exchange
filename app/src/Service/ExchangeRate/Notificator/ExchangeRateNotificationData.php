<?php
declare(strict_types=1);

namespace App\Service\ExchangeRate\Notificator;

use App\Entity\ClientSubscription;
use App\Entity\ExchangeRate;

class ExchangeRateNotificationData
{
    private ExchangeRate $rate;

    private ClientSubscription $subscription;

    public function __construct(ExchangeRate $rate, ClientSubscription $subscription)
    {
        $this->rate = $rate;
        $this->subscription = $subscription;
    }

    public function getRate(): float
    {
        return $this->rate->getSellRate();
    }

    public function getCode(): string
    {
        return $this->rate->getCode();
    }

    public function getCurrencyName(): string
    {
        
        return $this->subscription->getCurrency()->getName();
    }

    public function isLow(): bool
    {
        return $this->getRate() <= $this->subscription->getLowRateAlert();
    }

    public function isHigh(): bool
    {
        return $this->getRate() >= $this->subscription->getHighRateAlert();
    }
}