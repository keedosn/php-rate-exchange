<?php

declare(strict_types=1);

namespace App\Service\ExchangeRate;

use App\Service\ExchangeRate\FetchAdapter\FetchAdapterInterface;

class ExchangeRateFetcher
{
    private array $adapters = [];
 
    public function addAdapter(FetchAdapterInterface $adapter): void
    {
        $this->adapters[] = $adapter;
    }

    public function fetch(string $name): array
    {
        $data = [];
        foreach ($this->adapters as $adapter) {
            if ($adapter->name !== $name) {
                continue;
            }
            
            $data = $adapter->fetch();
        }

        return $data;
    }
}