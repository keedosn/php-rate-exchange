<?php

declare(strict_types=1);

namespace App\Service\ExchangeRate\FetchAdapter;

interface FetchAdapterInterface
{
    public const SERVICE_TAG = 'app.rate_adapter';

    public function fetch(): array;
}
