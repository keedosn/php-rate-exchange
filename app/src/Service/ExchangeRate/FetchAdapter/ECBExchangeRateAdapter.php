<?php

declare(strict_types=1);

namespace App\Service\ExchangeRate\FetchAdapter;

use App\Entity\Currency;
use Doctrine\Persistence\ObjectManager;
use GuzzleHttp\Exception\RequestException;

class ECBExchangeRateAdapter implements FetchAdapterInterface
{
    public $name = self::NAME;
    
    private string $defaultCurrency = 'PLN';

    private ObjectManager $em;

    private ApiClient $apiClient;

    const API_URL = 'https://sdw-wsrest.ecb.europa.eu/service/data/EXR/D.%s.%s.SP00.A';

    const NAME = 'ecb';

    public function __construct(ObjectManager $em, ApiClient $apiClient)
    {
        $this->em = $em;
        $this->apiClient = $apiClient;
    }

    public function fetch(): array
    {
        $now = new \DateTime();
        $currencies = $this->em->getRepository(Currency::class)->findAll();
        $rates = [];
        /** @var Currency $currency */
        foreach ($currencies as $currency) {
            if ($currency->getCode() == $this->defaultCurrency) {
                continue;
            }
            
            if (!($data = $this->getCurrencyRateData($currency->getCode()))) {
                continue;
            }

            $rates[] = [
                'name' => $currency->getName(),
                'code' => $currency->getCode(),
                'buy_rate' => $data['rate'],
                'sell_rate' => $data['rate'],
                'date' => $data['date']
            ];
        }
        
        return $rates;
    }

    private function getCurrencyRateData(string $currencyCode)
    {
        $now = new \DateTime();
        $yesterday = (new \DateTime())->modify('-1 day');

        $options = [
            'query' => [
                'startPeriod' => $yesterday->format('Y-m-d'),
                'endPeriod'   => $now->format('Y-m-d'),
            ],
            'headers' => [
                'Accept' => 'application/json',
            ],
        ];

        $response = $this->apiClient->makeRequest($this->buildUrl($currencyCode), 'GET', $options);
        if (!$response) {
            return false;
        }

        $data = json_decode($response);
        if (null === $data) {
            return null;
        }

        $date = new \DateTime($data->header->prepared);

        $rate = null;
        foreach ($data->dataSets as $dataSet) {
            $firstElem = $dataSet->series->{'0:0:0:0:0'}; // So sick!
            $obs = $firstElem->observations;

            if (!is_array($obs->{0})) {
                continue;
            }

            $rate = $obs->{0}[0];
            break;
        }

        return [
            'date' => $date->format('Y-m-d'),
            'rate' => $rate
        ];
    }

    private function buildUrl(string $targetCurrencyCode)
    {
        return sprintf(static::API_URL, $this->defaultCurrency, $targetCurrencyCode);
    }
}
