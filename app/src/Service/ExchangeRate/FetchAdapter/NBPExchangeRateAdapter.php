<?php

declare(strict_types=1);

namespace App\Service\ExchangeRate\FetchAdapter;

use App\Entity\Currency;
use Doctrine\Persistence\ObjectManager;

class NBPExchangeRateAdapter implements FetchAdapterInterface
{
    public $name = self::NAME;
    
    private ObjectManager $em;

    private ApiClient $apiClient;

    const API_URL = 'https://api.nbp.pl/api/exchangerates/tables/c/?format=xml';

    const NAME = 'nbp';

    public function __construct(ObjectManager $em, ApiClient $apiClient)
    {
        $this->em = $em;
        $this->apiClient = $apiClient;
    }

    public function fetch(): array
    {
        $currencies = $this->em->getRepository(Currency::class)->findAll();
        $availableCurrencies = [];
        foreach ($currencies as $currency) {
            $availableCurrencies[] = $currency->getCode();
        }
        $response = $this->apiClient->makeRequest(static::API_URL);
        $xmlObj = simplexml_load_string($response);

        $date = (string)$xmlObj->ExchangeRatesTable->EffectiveDate;
        $rates = $xmlObj->ExchangeRatesTable->Rates->Rate;

        $data = [];
        foreach ($rates as $rate) {
            if (true !== in_array($rate->Code, $availableCurrencies)) {
                continue;
            }
            
            $data[] = [
                'name' => (string)$rate->Currency,
                'code' => (string)$rate->Code,
                'buy_rate' => (float)$rate->Bid,
                'sell_rate' => (float)$rate->Ask,
                'date' => $date,
            ];
        }
        
        return $data;
    }
}
