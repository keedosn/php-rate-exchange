<?php

declare(strict_types=1);

namespace App\Service\ExchangeRate\FetchAdapter;

use GuzzleHttp\Exception\RequestException;

class ApiClient
{
    public function makeRequest(string $url, $method = 'GET', $options = [])
    {
        $client = new \GuzzleHttp\Client();

        try {
            $response = $client->request($method, $url, $options);
        }
        catch (RequestException $e) {
            return false;
        }

        return $response->getBody()->getContents();
    }
}
