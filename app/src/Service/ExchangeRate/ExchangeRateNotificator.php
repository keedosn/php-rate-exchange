<?php
declare(strict_types=1);

namespace App\Service\ExchangeRate;

use App\Entity\Client;
use App\Entity\ClientSubscription;
use App\Entity\ExchangeRate;
use App\Service\ExchangeRate\Notificator\ExchangeRateNotificationData;
use App\Service\ExchangeRate\Notificator\SubscriptionMessageBuilder;
use App\Service\SubscriptionService;
use Doctrine\Persistence\ObjectManager;

class ExchangeRateNotificator
{
    private SubscriptionService $subService;

    private ObjectManager $em;

    private SubscriptionMessageBuilder $messageBuilder;

    private \Swift_Mailer $mailer;

    public function __construct(SubscriptionService $subService, ObjectManager $em, SubscriptionMessageBuilder $messageBuilder, \Swift_Mailer $mailer)
    {
        $this->subService = $subService;
        $this->em = $em;
        $this->messageBuilder = $messageBuilder;
        $this->mailer = $mailer;
    }

    public function sendNotifications(Client $client, \DateTime $date): bool
    {
        $subscriptions = $this->subService->getSubscriptionsForClient($client);
        if (0 === count($subscriptions)) {
            return false;
        }
        
        $notificationData = [];
        /** @var ClientSubscription $subscription */
        foreach ($subscriptions as $subscription) {
            $rate = $this->em->getRepository(ExchangeRate::class)->findOneBy(['code' => $subscription->getCurrency()->getCode(), 'date' => $date]);
            if (null === $rate) {
                continue;
            }

            $notificationData[] = new ExchangeRateNotificationData($rate, $subscription);
        }

        if (0 === count($notificationData)) {
            return false;
        }
        
        $subscriptionMessage = $this->messageBuilder->prepare($notificationData, $client, $date);
        
        return (bool)$this->sendEmail($client, $subscriptionMessage->getMessageBody());
    }

    private function sendEmail(Client $client, string $messageBody): int
    {
        $message = (new \Swift_Message('Your daily exchange rates subscription just arrived!'))
            ->setFrom('mailer@localhost')
            ->setTo($client->getEmail(), $client->getFullName())
            ->setBody($messageBody, 'text/html')
        ;

        return $this->mailer->send($message);
    }
}