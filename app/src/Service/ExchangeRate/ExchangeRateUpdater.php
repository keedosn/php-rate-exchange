<?php

declare(strict_types=1);

namespace App\Service\ExchangeRate;

use App\Entity\ExchangeRate;
use Doctrine\Persistence\ObjectManager;

class ExchangeRateUpdater
{
    private ObjectManager $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    public function update(array $data, string $type): ExchangeRate
    {
        $filterParams = ['source' => $type, 'code' => $data['code'], 'date' => new \DateTime($data['date'])];
        $rate = $this->em->getRepository(ExchangeRate::class)->findOneBy($filterParams);
        if (!$rate) {
            $rate = new ExchangeRate();
            $rate->setSource($type);
            $rate->setCode($data['code']);
            $rate->setRatio(1);
        }

        $rate->setBuyRate($data['buy_rate']);
        $rate->setSellRate($data['sell_rate']);
        $rate->setDate(new \DateTime($data['date']));

        $this->em->persist($rate);
        $this->em->flush();

        return $rate;
    }
}
