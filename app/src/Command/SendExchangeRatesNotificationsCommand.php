<?php

namespace App\Command;

use App\Entity\Client;
use App\Service\ExchangeRate\ExchangeRateNotificator;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SendExchangeRatesNotificationsCommand extends Command
{
    protected static $defaultName = 'app:exchange-rates:send';

    private ObjectManager $em;
    
    private ExchangeRateNotificator $notificator;

    public function __construct(ObjectManager $em, ExchangeRateNotificator $notificator)
    {
        parent::__construct();

        $this->em = $em;
        $this->notificator = $notificator;
    }

    protected function configure()
    {
        $this
            ->setDescription('Notify about exchange rates changes')
            ->addOption('date', 'd', InputOption::VALUE_OPTIONAL, 'Select date(Y-m-d format) for exchange rates to notify')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $date = $input->getOption('date') ?? (new \DateTime())->format('Y-m-d');
        $dt = new \DateTime($date);
        $sent = 0;
        $clients = $this->em->getRepository(Client::class)->findByStatus(Client::STATUS_ACTIVE);
        foreach ($clients as $client) {
            if ($this->notificator->sendNotifications($client, $dt)) {
                $sent++;
            }
        }

        $io = new SymfonyStyle($input, $output);
        $io->success(sprintf('Sent %d notification emails', $sent));

        return 0;
    }
}
