<?php

namespace App\Command;

use App\Service\ExchangeRate\ExchangeRateFetcher;
use App\Service\ExchangeRate\ExchangeRateUpdater;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SyncExchangeRatesCommand extends Command
{
    protected static $defaultName = 'app:exchange-rates:sync';

    private $defaultSource = 'nbp';

    private ExchangeRateFetcher $fetcher;

    private ExchangeRateUpdater $updater;

    public function __construct(ExchangeRateFetcher $fetcher, ExchangeRateUpdater $updater)
    {
        parent::__construct();

        $this->fetcher = $fetcher;
        $this->updater = $updater;
    }

    protected function configure()
    {
        $this
            ->setDescription('Sync exchange rates')
            ->addOption('source', 's', InputOption::VALUE_REQUIRED, 'Select source for exchange rates sync')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $source = $input->getOption('source') ?? $this->defaultSource;
        $exchangeRates = $this->fetcher->fetch($source);
        
        $i = 0;
        foreach ($exchangeRates as $exchangeRate) {
            $successful = $this->updater->update($exchangeRate, $source);
            if ($successful) {
                $i++;
            }
        }

        $io = new SymfonyStyle($input, $output);
        $io->success(sprintf('Updated %d rates exchanges', $i));

        return 0;
    }
}
