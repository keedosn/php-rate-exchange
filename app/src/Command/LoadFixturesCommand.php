<?php

declare(strict_types=1);

namespace App\Command;

use Fidry\AliceDataFixtures\Persistence\PurgeMode;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadFixturesCommand extends Command
{
    const FIXTURE_PATH = 'src/DataFixtures/dev/';

    protected static $defaultName = 'app:fixtures:load';

    public ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Load single development fixture file');
        $this->addArgument('file', InputArgument::REQUIRED, 'Fixture file name or list of files separated with space. For example: app:fixtures:load currency.yml');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $loader = $this->container->get('fidry_alice_data_fixtures.loader.doctrine');

        $files = explode(' ', $input->getArgument('file'));
        $filePaths = preg_filter('/^/', self::FIXTURE_PATH, $files);

        $loader->load($filePaths, [], [], PurgeMode::createNoPurgeMode());

        $output->writeln('Fixture was loaded.');

        return 0;
    }
}
