<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\ClientSubscription;
use App\Event\ClientConfirmedRegistrationEvent;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class WebController extends AbstractController
{
    private ObjectManager $em;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(ObjectManager $em, EventDispatcherInterface $eventDispatcher)
    {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/confirm/{token}", methods={"GET"}, name="confirm_regisration")
     */
    public function confirmRegistration(Request $request, string $token): Response
    {
        $client = $this->em->getRepository(Client::class)->findOneByConfirmationToken($token);
        if (!$client) {
            return $this->json(['message' => 'Client not found'], Response::HTTP_NOT_FOUND);
        }

        if ($client->getStatus() == Client::STATUS_ACTIVE) {
            return $this->json(['message' => 'Client already confirmed'], Response::HTTP_BAD_REQUEST);
        }

        $client->setStatus(Client::STATUS_ACTIVE);
        $this->em->persist($client);
        $this->em->flush();

        $this->eventDispatcher->dispatch(new ClientConfirmedRegistrationEvent($client), ClientConfirmedRegistrationEvent::NAME);

        return $this->render('web/registration_confirmation.html.twig', []);
    }

    /**
     * @Route("/unsubscribe", methods={"GET"}, name="notification_unsubscribe")
     */
    public function notificationUnsubscribe(Request $request): Response
    {
        $tokens = $request->query->get('t');
        foreach ($tokens as $token) {
            $subscription = $this->em->getRepository(ClientSubscription::class)->findOneByUnsubscribeToken($token);
            if (null === $subscription) {
                continue;
            }

            $this->em->remove($subscription);
        }

        $this->em->flush();

        return $this->render('web/unsubscribe_confirmation.html.twig', []);
    }
}
