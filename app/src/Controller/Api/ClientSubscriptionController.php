<?php

namespace App\Controller\Api;

use App\Entity\ClientSubscription;
use App\Entity\Currency;
use App\Exception\CurrencyNotFoundException;
use App\Exception\SubscriptionNotFoundException;
use App\Service\SubscriptionService;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ClientSubscriptionController extends AbstractController
{
    private ObjectManager $em;

    private SerializerInterface $serializer;

    private ValidatorInterface $validator;

    private SubscriptionService $subService;

    public function __construct(ObjectManager $em, SerializerInterface $serializer, ValidatorInterface $validator, SubscriptionService $subService)
    {
        $this->em = $em;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->subService = $subService;
    }

    /**
     * @Route("/subscription", methods={"GET"}, name="get_subscriptions")
     */
    public function getSubscriptions(Request $request): Response
    {
        $collection = $this->em->getRepository(ClientSubscription::class)->findBy(['client' => $this->getUser()]);

        return $this->json($collection, Response::HTTP_OK, [], ['groups' => ['ClientSubscription', 'Id']]);
    }

    /**
     * @Route("/subscription", methods={"POST"}, name="subscribe")
     */
    public function subscribe(Request $request): Response
    {
        $client = $this->getUser();

        $subscriptionData = $this->serializer->decode($request->getContent(), 'json');
        foreach ($subscriptionData as $row) {
            /** @var ClientSubscription $subscription  */
            $subscription = $this->serializer->denormalize($row, ClientSubscription::class);
            if (!isset($row['currencyCode'])) {
                return $this->json(['message' => 'Missing currencyCode param'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            
            $errors = $this->validator->validate($subscription, null, ['Subscribe']);
            if (count($errors) > 0) {
                return $this->json(['message' => (string)$errors], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            try {
                $currency = $this->em->getRepository(Currency::class)->findOneByCode($row['currencyCode']);
                if (!$currency) {
                    continue;
                }
                $this->subService->addSubscription(
                    $client, $currency, $subscription->getLowRateAlert(), $subscription->getHighRateAlert()
                );
            } catch(CurrencyNotFoundException $e) {
                continue;
            }
        }
        $this->em->flush();

        $response = $this->subService->getSubscriptionsForClient($client);

        return $this->json($response, Response::HTTP_OK, [], ['groups' => ['ClientSubscription', 'Id']]);
    }

    /**
     * @Route("/subscription", methods={"DELETE"}, name="unsubscribe")
     */
    public function unsubscribe(Request $request): Response
    {
        $client = $this->getUser();

        $subscriptionData = $this->serializer->decode($request->getContent(), 'json');
        /** @var ClientSubscription $subscription  */
        $subscription = $this->serializer->denormalize($subscriptionData, ClientSubscription::class);
        $errors = $this->validator->validate($subscription, null, ['Unsubscribe']);
        if (count($errors) > 0) {
            return $this->json(['message' => (string)$errors], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $currency = $this->em->getRepository(Currency::class)->findOneByCode($subscriptionData['currencyCode']);
            if (null === $currency) {
                return $this->json(['message' => 'Currency not exists'], Response::HTTP_NOT_FOUND);
            }
            $this->subService->removeSubscription($client, $currency, $subscription->getUnsubscribeToken());
        } catch (SubscriptionNotFoundException $e) {
            return $this->json(['message' => 'Subscription not found'], Response::HTTP_NOT_FOUND);
        }

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
