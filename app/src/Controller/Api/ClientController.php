<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Entity\Client;
use App\Entity\Currency;
use App\Event\ClientRegisteredEvent;
use App\Exception\CurrencyNotFoundException;
use App\Service\SubscriptionService;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ClientController extends AbstractController
{
    private ObjectManager $em;

    private SerializerInterface $serializer;

    private SubscriptionService $subService;

    private ValidatorInterface $validator;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ObjectManager $em, SerializerInterface $serializer, SubscriptionService $subService,
        ValidatorInterface $validator, EventDispatcherInterface $eventDispatcher
    ) {
        $this->em = $em;
        $this->serializer = $serializer;
        $this->subService = $subService;
        $this->validator = $validator;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/client", methods={"POST"}, name="client_register")
     */
    public function register(Request $request): Response
    {
        $data = $this->serializer->decode($request->getContent(), 'json');
        $client = $this->serializer->denormalize($data, Client::class);
        $subscribingCurrencies = $data['currencies'] ?? [];

        $errors = $this->validator->validate($client, null, ['Register']);
        if (count($errors) > 0) {
            return $this->json(['message' => (string)$errors], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        foreach ($subscribingCurrencies as $row) {
            try {
                $currency = $this->em->getRepository(Currency::class)->findOneByCode($row['currencyCode']);
                if (!$currency) {
                    continue;
                }
                $this->subService->addSubscription($client, $currency, (float)$row['lowRateAlert'], (float)$row['highRateAlert']);
            }
            catch (CurrencyNotFoundException $e) {
                continue;
            }
        }

        $this->em->persist($client);
        $this->em->flush();

        $this->eventDispatcher->dispatch(new ClientRegisteredEvent($client), ClientRegisteredEvent::NAME);

        return $this->json($client, Response::HTTP_OK, [], ['groups' => ['Client']]);
    }
}
