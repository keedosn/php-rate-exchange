<?php

declare(strict_types=1);

namespace App\DependencyInjection\Compiler;

use App\Service\ExchangeRate\ExchangeRateFetcher;
use App\Service\ExchangeRate\FetchAdapter\FetchAdapterInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class RateAdapterPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(ExchangeRateFetcher::class)) {
            return;
        }

        $definition = $container->findDefinition(ExchangeRateFetcher::class);
        $taggedServices = $container->findTaggedServiceIds(FetchAdapterInterface::SERVICE_TAG);

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addAdapter', [new Reference($id)]);
        }
    }
}