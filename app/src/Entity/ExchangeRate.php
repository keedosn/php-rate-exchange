<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ExchangeRate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="string", length=36, nullable=false, options={"fixed"=true})
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private string $source;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private string $code;

    /**
     * @ORM\Column(type="integer")
     */
    private int $ratio = 1;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private float $buyRate;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private float $sellRate;

    /**
     * @ORM\Column(type="date")
     */
    private \DateTime $date;

    public function getId(): string
    {
        return $this->id;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function setSource(string $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getRatio(): int
    {
        return $this->ratio;
    }

    public function setRatio(int $ratio): self
    {
        $this->ratio = $ratio;

        return $this;
    }

    public function getBuyRate(): float
    {
        return $this->buyRate;
    }

    public function setBuyRate(float $buyRate): self
    {
        $this->buyRate = $buyRate;

        return $this;
    }

    public function getSellRate(): float
    {
        return $this->sellRate;
    }

    public function setSellRate(float $sellRate): self
    {
        $this->sellRate = $sellRate;

        return $this;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }
}
