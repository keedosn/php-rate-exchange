<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Faker\Provider\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation as Serialization;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class ClientSubscription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="string", length=36, nullable=false, options={"fixed"=true})
     * @Serialization\Groups({"Id", "ClientSubscription"})
     */
    private string $id;

    /**
     * @ORM\ManyToOne(targetEntity="Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * @Serialization\Groups({"ClientSubscription"})
     */
    private Client $client;

    /**
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     * @Serialization\Groups({"ClientSubscription"})
     */
    private Currency $currency;

    /**
     * @ORM\Column(name="low_rate_alert", type="decimal", precision=8, scale=2)
     * @Assert\NotBlank(groups={"Subscribe"})
     * @Serialization\Groups({"ClientSubscription"})
     */
    private float $lowRateAlert;

    /**
     * @ORM\Column(name="high_rate_alert", type="decimal", precision=8, scale=2)
     * @Assert\NotBlank(groups={"Subscribe"})
     * @Serialization\Groups({"ClientSubscription"})
     */
    private float $highRateAlert;

    /**
     * @ORM\Column(name="unsubscribe_token", type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     * @Assert\NotBlank(groups={"Unsubscribe"})
     * @Assert\Uuid(groups={"Unsubscribe"})
     * @Serialization\Groups({"ClientSubscription"})
     */
    private string $unsubscribeToken;

    /**
     * @ORM\PrePersist
     */
    public function generateUnsubscribeToken()
    {
        $this->unsubscribeToken = Uuid::uuid();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function setClient(Client $client): ClientSubscription
    {
        $this->client = $client;

        return $this;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    public function setCurrency(Currency $currency): ClientSubscription
    {
        $this->currency = $currency;

        return $this;
    }

    public function getLowRateAlert(): float
    {
        return $this->lowRateAlert;
    }

    public function setLowRateAlert(float $lowRateAlert): ClientSubscription
    {
        $this->lowRateAlert = $lowRateAlert;

        return $this;
    }

    public function getHighRateAlert(): float
    {
        return $this->highRateAlert;
    }

    public function setHighRateAlert(float $highRateAlert): ClientSubscription
    {
        $this->highRateAlert = $highRateAlert;

        return $this;
    }

    public function getUnsubscribeToken(): string
    {
        return $this->unsubscribeToken;
    }

    public function setUnsubscribeToken(string $unsubscribeToken): ClientSubscription
    {
        $this->unsubscribeToken = $unsubscribeToken;

        return $this;
    }
}
