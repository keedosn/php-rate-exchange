<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Faker\Provider\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation as Serialization;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("email", groups={"Register"})
 */
class Client implements UserInterface
{
    const STATUS_INACTIVE = 'inactive';
    
    const STATUS_ACTIVE = 'active';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     * @ORM\Column(type="string", length=36, nullable=false, options={"fixed"=true})
     * @Serialization\Groups({"Client", "Id"})
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Assert\NotBlank(groups={"Register"})
     * @Assert\Email(groups={"Register"})
     * @Serialization\Groups({"Client"})
     */
    private string $email;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Assert\NotBlank(groups={"Register"})
     * @Serialization\Groups({"Client"})
     */
    private string $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     * @Assert\NotBlank(groups={"Register"})
     * @Serialization\Groups({"Client"})
     */
    private string $lastName;

    /**
     * @ORM\Column(type="integer", length=9)
     * 
     * @Assert\NotBlank(groups={"Register"})
     * @Assert\Positive(groups={"Register"})
     * @Assert\Length(min=9, max=9, groups={"Register"})
     * @Serialization\Groups({"Client"})
     */
    private int $phoneNumber;

    /**
     * @ORM\Column(type="date")
     * 
     * @Assert\NotBlank(groups={"Register"})
     * @Assert\Date(groups={"Register"})
     * @Assert\LessThan("-18 years", groups={"Register"})
     * @Serialization\Groups({"Client"})
     */
    private \DateTime $dob;

    /**
     * @ORM\Column(type="string", length=10)
     * @Serialization\Groups({"Client"})
     */
    private string $status = self::STATUS_INACTIVE;

    /**
     * @ORM\Column(type="string", length=50)
     * @Serialization\Groups({"Client"})
     */
    private string $confirmationToken;

    /**
     * @ORM\Column(type="string", length=36, unique=true, nullable=false)
     * @Serialization\Groups({"Client"})
     */
    private string $apiToken;

    private array $roles = ['ROLE_USER'];
    
    private ?string $password;

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->confirmationToken = Uuid::uuid();
        $this->apiToken = Uuid::uuid();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): Client
    {
        $this->email = $email;

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): Client
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): Client
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFullName(): string
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }

    public function getPhoneNumber(): int
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(int $phoneNumber): Client
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getDob(): \DateTime
    {
        return $this->dob;
    }

    public function setDob(\DateTime $dob): Client
    {
        $this->dob = $dob;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): Client
    {
        $this->status = $status;

        return $this;
    }

    public function getConfirmationToken(): string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(string $confirmationToken): Client
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function getApiToken(): string
    {
        return $this->apiToken;
    }

    public function getRoles()
    {
        return array_unique($this->roles);
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        $this->password = null;
    }
}
