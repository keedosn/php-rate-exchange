<?php

declare(strict_types=1);

namespace App\Exception;

class SubscriptionNotFoundException extends \Exception
{
}
