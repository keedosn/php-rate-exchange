<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201126230711 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE client (id CHAR(36) NOT NULL, email VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, phone_number INT NOT NULL, dob DATE NOT NULL, status VARCHAR(10) NOT NULL, confirmation_token VARCHAR(50) NOT NULL, api_token VARCHAR(36) NOT NULL, UNIQUE INDEX UNIQ_C74404557BA2F5EB (api_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client_subscription (id CHAR(36) NOT NULL, client_id CHAR(36) DEFAULT NULL, currency_id CHAR(36) DEFAULT NULL, low_rate_alert NUMERIC(8, 2) NOT NULL, high_rate_alert NUMERIC(8, 2) NOT NULL, unsubscribe_token VARCHAR(255) NOT NULL, INDEX IDX_12CCC6D219EB6921 (client_id), INDEX IDX_12CCC6D238248176 (currency_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE currency (id CHAR(36) NOT NULL, name VARCHAR(100) NOT NULL, code VARCHAR(3) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE exchange_rate (id CHAR(36) NOT NULL, source VARCHAR(5) NOT NULL, code VARCHAR(3) NOT NULL, ratio INT NOT NULL, buy_rate NUMERIC(8, 2) NOT NULL, sell_rate NUMERIC(8, 2) NOT NULL, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE client_subscription ADD CONSTRAINT FK_12CCC6D219EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_subscription ADD CONSTRAINT FK_12CCC6D238248176 FOREIGN KEY (currency_id) REFERENCES currency (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE client_subscription DROP FOREIGN KEY FK_12CCC6D219EB6921');
        $this->addSql('ALTER TABLE client_subscription DROP FOREIGN KEY FK_12CCC6D238248176');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE client_subscription');
        $this->addSql('DROP TABLE currency');
        $this->addSql('DROP TABLE exchange_rate');
    }
}
