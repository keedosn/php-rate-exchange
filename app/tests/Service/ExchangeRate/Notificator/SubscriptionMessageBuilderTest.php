<?php
declare(strict_types=1);

namespace App\Tests\Service\ExchangeRate\Notificator;

use App\Service\ExchangeRate\Notificator\SubscriptionMessageBuilder;
use App\Service\SubscriptionService;
use App\Tests\FixturesTestCase;

class SubscriptionMessageBuilderTest extends FixturesTestCase
{
    public function testSubscriptionMessageBuilderCreation()
    {
        $subService = $this->createMock(SubscriptionService::class);
        $twig = $this->getContainer()->get('twig');
        $appUrl = $this->getContainer()->getParameter('app.url'); //http://localhost:8000
        $builder = new SubscriptionMessageBuilder($subService, $twig, $appUrl);

        $data = [];
        $client = $this->getFixtures()->getFixtureObject('client1');
        $dt = new \DateTime();
        $builder->prepare($data, $client, $dt);

        $this->assertSame([], $builder->getData());
        $this->assertSame($client, $builder->getClient());
        $this->assertSame($dt, $builder->getDate());
    }
}