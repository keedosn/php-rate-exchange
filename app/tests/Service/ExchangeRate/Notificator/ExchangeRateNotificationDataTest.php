<?php
declare(strict_types=1);

namespace App\Tests\Service\ExchangeRate\Notificator;

use App\Service\ExchangeRate\Notificator\ExchangeRateNotificationData;
use App\Tests\FixturesTestCase;

class ExchangeRateNotificationDataTest extends FixturesTestCase
{
    public function testExchangeRateNotificationDataWithTypicalExchangeRate()
    {
        $exchangeRate = $this->getFixtures()->getFixtureObject('rate_euro_3');
        $subscription = $this->getFixtures()->getFixtureObject('client1_eur');
        $notificationData = new ExchangeRateNotificationData($exchangeRate, $subscription);

        $this->assertSame('EUR', $notificationData->getCode());
        $this->assertSame('Euro', $notificationData->getCurrencyName());
        $this->assertSame(4.364, $notificationData->getRate());
        $this->assertSame(false, $notificationData->isLow());
        $this->assertSame(false, $notificationData->isHigh());
    }

    public function testExchangeRateNotificationDataWithLowExchangeRate()
    {
        $exchangeRate = $this->getFixtures()->getFixtureObject('rate_euro_2');
        $subscription = $this->getFixtures()->getFixtureObject('client1_eur');
        $notificationData = new ExchangeRateNotificationData($exchangeRate, $subscription);

        $this->assertSame('EUR', $notificationData->getCode());
        $this->assertSame('Euro', $notificationData->getCurrencyName());
        $this->assertSame(4.0, $notificationData->getRate());
        $this->assertSame(true, $notificationData->isLow());
        $this->assertSame(false, $notificationData->isHigh());
    }

    public function testExchangeRateNotificationDataWithHighExchangeRate()
    {
        $exchangeRate = $this->getFixtures()->getFixtureObject('rate_euro_1');
        $subscription = $this->getFixtures()->getFixtureObject('client1_eur');
        $notificationData = new ExchangeRateNotificationData($exchangeRate, $subscription);

        $this->assertSame('EUR', $notificationData->getCode());
        $this->assertSame('Euro', $notificationData->getCurrencyName());
        $this->assertSame(4.424, $notificationData->getRate());
        $this->assertSame(false, $notificationData->isLow());
        $this->assertSame(true, $notificationData->isHigh());
    }
}