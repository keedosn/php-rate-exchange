<?php
declare(strict_types=1);

namespace App\Tests\Service\ExchangeRate;

use App\Service\ExchangeRate\ExchangeRateNotificator;
use App\Service\ExchangeRate\Notificator\SubscriptionMessageBuilder;
use App\Service\SubscriptionService;
use App\Tests\FixturesTestCase;

class ExchangeRateNotificatorTest extends FixturesTestCase
{
    public function testDoNothingWhenMissingSubscriptions()
    {
        $subService = $this->createMock(SubscriptionService::class);
        $subService->expects($this->any())->method('getSubscriptionsForClient')->willReturn([]);
        $twig = $this->getContainer()->get('twig');
        $appUrl = $this->getContainer()->getParameter('app.url');

        $notificator = new ExchangeRateNotificator(
            $subService, 
            $this->getContainer()->get('doctrine.orm.entity_manager'), 
            new SubscriptionMessageBuilder($subService, $twig, $appUrl), 
            $this->getContainer()->get('mailer')
        );

        $client = $this->getFixtures()->getFixtureObject('client1');
        $returnValue = $notificator->sendNotifications($client, new \DateTime('2020-11-27'));

        $this->assertSame(false, $returnValue);
    }

    public function testDoNothingWhenMissingExchangeRates()
    {
        $subscriptions = [
            $this->getFixtures()->getFixtureObject('client1_eur'),
            $this->getFixtures()->getFixtureObject('client1_usd'),
            $this->getFixtures()->getFixtureObject('client1_gbp'),
        ];
        $subService = $this->createMock(SubscriptionService::class);
        $subService->expects($this->any())->method('getSubscriptionsForClient')->willReturn($subscriptions);
        $twig = $this->getContainer()->get('twig');
        $appUrl = $this->getContainer()->getParameter('app.url');

        $notificator = new ExchangeRateNotificator(
            $subService, 
            $this->getContainer()->get('doctrine.orm.entity_manager'), 
            new SubscriptionMessageBuilder($subService, $twig, $appUrl), 
            $this->getContainer()->get('mailer')
        );

        $client = $this->getFixtures()->getFixtureObject('client1');
        $returnValue = $notificator->sendNotifications($client, new \DateTime('2020-11-24'));

        $this->assertSame(false, $returnValue);
    }

    public function testSendNotifications()
    {
        $subscriptions = [
            $this->getFixtures()->getFixtureObject('client1_eur'),
            $this->getFixtures()->getFixtureObject('client1_usd'),
            $this->getFixtures()->getFixtureObject('client1_gbp'),
        ];
        $subService = $this->createMock(SubscriptionService::class);
        $subService->expects($this->any())->method('getSubscriptionsForClient')->willReturn($subscriptions);
        $twig = $this->getContainer()->get('twig');
        $appUrl = $this->getContainer()->getParameter('app.url');
        $mailer = $this->createMock(\Swift_Mailer::class);
        $mailer->expects($spy = $this->any())->method('send')->willReturn(1);

        $notificator = new ExchangeRateNotificator(
            $subService, 
            $this->getContainer()->get('doctrine.orm.entity_manager'), 
            new SubscriptionMessageBuilder($subService, $twig, $appUrl), 
            $mailer
        );

        $client = $this->getFixtures()->getFixtureObject('client1');
        $notificator->sendNotifications($client, new \DateTime('2020-11-27'));

        $this->assertSame(1, $spy->getInvocationCount());
    }
}