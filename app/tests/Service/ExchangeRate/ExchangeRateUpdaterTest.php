<?php
declare(strict_types=1);

namespace App\Tests\Service\ExchangeRate;

use App\Entity\ExchangeRate;
use App\Service\ExchangeRate\ExchangeRateUpdater;
use App\Tests\FixturesTestCase;
use Doctrine\Persistence\ObjectManager;

class ExchangeRateUpdaterTest extends FixturesTestCase
{
    public function testCreateNewRate()
    {
        /** @var ObjectManager $em */
        $updater = new ExchangeRateUpdater(
            $em = $this->getContainer()->get('doctrine.orm.entity_manager')
        );

        // $prevRate = $this->getFixtures()->getFixtureObject('rate_euro_3'); // eur exchange rate for 2020-11-27
        $data = [
            'code' => 'EUR',
            'buy_rate' => 4.50,
            'sell_rate' => 4.25,
            'date' => '2020-11-28'
        ];
        $newRate = $updater->update($data, 'nbp');
        $rate = $em->getRepository(ExchangeRate::class)->find($newRate->getId());

        $this->assertSame('EUR', $rate->getCode());
        $this->assertSame(4.50, $rate->getBuyRate());
        $this->assertSame(4.25, $rate->getSellRate());
        $this->assertSame('2020-11-28', $rate->getDate()->format('Y-m-d'));
    }

    public function testUpdateExistingRate()
    {
        /** @var ObjectManager $em */
        $updater = new ExchangeRateUpdater(
            $em = $this->getContainer()->get('doctrine.orm.entity_manager')
        );

        $prevRate = $this->getFixtures()->getFixtureObject('rate_euro_3'); // eur exchange rate for 2020-11-27
        $data = [
            'code' => 'EUR',
            'buy_rate' => 4.44,
            'sell_rate' => 4.33,
            'date' => '2020-11-27'
        ];
        $updater->update($data, 'nbp');
        $updatedRate = $em->getRepository(ExchangeRate::class)->find($prevRate->getId());

        $this->assertSame('EUR', $updatedRate->getCode());
        $this->assertSame(4.44, $updatedRate->getBuyRate());
        $this->assertSame(4.33, $updatedRate->getSellRate());
        $this->assertSame('2020-11-27', $updatedRate->getDate()->format('Y-m-d'));
    }
}