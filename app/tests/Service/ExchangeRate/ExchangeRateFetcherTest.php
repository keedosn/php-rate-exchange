<?php
declare(strict_types=1);

namespace App\Tests\Service\ExchangeRate;

use App\Service\ExchangeRate\ExchangeRateFetcher;
use App\Tests\FixturesTestCase;

class ExchangeRateFetcherTest extends FixturesTestCase
{
    public function testShouldReturnEmptyArrayWhenNoAdapterSpecified()
    {
        $fetcher = new ExchangeRateFetcher();

        $this->assertSame([], $fetcher->fetch('nbp'));
    }

    public function testShouldReturnEmptyArrayWhenNotExistingAdapterSpecified()
    {
        $fetcher = new ExchangeRateFetcher();

        $this->assertSame([], $fetcher->fetch('asdfg'));
    }
}