<?php
declare(strict_types=1);

namespace App\Tests\Service\ExchangeRate\FetchAdapter;

use App\Entity\Currency;
use App\Service\ExchangeRate\FetchAdapter\ApiClient;
use App\Service\ExchangeRate\FetchAdapter\ECBExchangeRateAdapter;
use App\Tests\FixturesTestCase;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;

class ECBExchangeRateAdapterTest extends FixturesTestCase
{
    public function testAdapterShouldReturnEmptyArrayWhenNoCurrenciesAvailable()
    {
        $currencyRepository = $this->createMock(ObjectRepository::class);
        $currencyRepository->expects($this->any())->method('findAll')->willReturn([]);
        $em = $this->createMock(ObjectManager::class);
        $em->expects($this->any())->method('getRepository')->willReturn($currencyRepository);
        $apiClient = new ApiClient();

        $ecbAdapter = new ECBExchangeRateAdapter($em, $apiClient);
        $data = $ecbAdapter->fetch();
        $this->assertSAme([], $data);
    }

    public function testAdapterFetchDataCorrectly()
    {
        $currency = new Currency();
        $currency->setName('Euro');
        $currency->setCode('EUR');
        $currencyRepository = $this->createMock(ObjectRepository::class);
        $currencyRepository->expects($this->any())->method('findAll')->willReturn([$currency]);
        $em = $this->createMock(ObjectManager::class);
        $em->expects($this->any())->method('getRepository')->willReturn($currencyRepository);
        $ecbApiResponse = file_get_contents('tests/data/ecb_adapter_api_response.txt');
        $apiClient = $this->createMock(ApiClient::class);
        $apiClient->expects($this->any())->method('makeRequest')->willReturn($ecbApiResponse);

        $ecbAdapter = new ECBExchangeRateAdapter($em, $apiClient);
        $data = $ecbAdapter->fetch();

        $this->assertSame('EUR', $data[0]['code']);
        $this->assertSame('Euro', $data[0]['name']);
        $this->assertSame(4.123, $data[0]['buy_rate']);
        $this->assertSame(4.123, $data[0]['sell_rate']);
        $this->assertSame('2020-11-27', $data[0]['date']);
    }
}