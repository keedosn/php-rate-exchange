<?php

declare(strict_types=1);

namespace App\Tests;

use App\Entity\Client;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class FixturesTestCase extends KernelTestCase
{
    private ObjectManager $entityManager;

    private static Fixtures $fixtures;

    public static function setUpBeforeClass(): void
    {
        self::$fixtures = Fixtures::getFixturesInstance();
    }

    protected function setUp(): void
    {
        static::bootKernel(['environment' => 'test']);
        $this->entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');
        $this->entityManager->beginTransaction();
        $this->entityManager->getConnection()->setAutoCommit(false);
    }

    protected function tearDown(): void
    {
        if ($this->entityManager->getConnection()->isTransactionActive()) {
            $this->entityManager->getConnection()->rollback();
            $this->entityManager->close();
        }

        static::ensureKernelShutdown();
    }

    protected function getFixtures(): Fixtures
    {
        return self::$fixtures;
    }

    protected function getContainer(): ContainerInterface
    {
        return static::$kernel->getContainer();
    }

    protected function getEntityManager(): ObjectManager
    {
        return $this->entityManager;
    }

    // some helpers
    protected function activateClient(string $fixtureName): void
    {
        /** @var Client $client */
        $clientFixture = $this->getFixtures()->getFixtureObject($fixtureName);
        $client = $this->getEntityManager()->getRepository(Client::class)->find($clientFixture->getId());
        $client->setStatus(Client::STATUS_ACTIVE);
        $this->getEntityManager()->persist($client);
        $this->getEntityManager()->flush();
    }
}
