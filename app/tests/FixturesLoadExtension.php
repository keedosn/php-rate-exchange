<?php

declare(strict_types=1);

namespace App\Tests;

use Fidry\AliceDataFixtures\Persistence\PurgeMode;
use PHPUnit\Runner\BeforeFirstTestHook;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

class FixturesLoadExtension extends KernelTestCase implements BeforeFirstTestHook
{
    public function executeBeforeFirstTest(): void
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $this->loadFixtures($kernel);
    }

    private function getFixtures(): iterable
    {
        return [
            'src/DataFixtures/tests/client.yml',
            'src/DataFixtures/tests/currency.yml',
            'src/DataFixtures/tests/clientSubscription.yml',
            'src/DataFixtures/tests/exchangeRate.yml',
        ];
    }

    private function loadFixtures(KernelInterface $kernel)
    {
        $loader = $kernel->getContainer()->get('fidry_alice_data_fixtures.loader.doctrine');
        $fixtures = $loader->load($this->getFixtures(), [], [], PurgeMode::createDeleteMode());
        Fixtures::createFixturesInstance($fixtures);
    }
}
