<?php
declare(strict_types=1);

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Response;

class WebTestCase extends FixturesTestCase
{
    private KernelBrowser $client;

    protected static string $defaultPath = '/api';

    protected string $defaultMethod = 'GET';

    protected function setUp(): void
    {
        parent::setUp();
        
        $kernel = static::$kernel;
        $this->client = $kernel->getContainer()->get('test.client');
        $this->client->disableReboot();
    }

    protected function getClientResponse($path = null, $method = null, $token = null, $body = null, $headers = []): Response
    {
        $path = ($path ?? static::$defaultPath);
        $method = $method ?? $this->defaultMethod;
        $headers = array_merge(['CONTENT_TYPE' => 'application/json'], $headers);

        if (null !== $token) {
            $headers['HTTP_X_AUTH_TOKEN'] = $token;
        }

        if (is_array($body)) {
            $body = json_encode($body);
        }
        
        $this->client->request($method, $path, [], [], $headers, $body);

        return $this->client->getResponse();
    }

    protected function getWebResponse($path = null, $method = null, $body = null, $headers = []): Response
    {
        return $this->getClientResponse($path, $method, null, $body, ['CONTENT_TYPE' => 'application/html']);
    }
    
    protected function checkJsonResponse(Response $response, int $statusCode = Response::HTTP_OK)
    {
        $this->assertEquals($statusCode, $response->getStatusCode());

        return json_decode($response->getContent());
    }

    protected function checkWebResponse(Response $response, int $statusCode = Response::HTTP_OK)
    {
        $this->assertEquals($statusCode, $response->getStatusCode());

        return $response->getContent();
    }
}
