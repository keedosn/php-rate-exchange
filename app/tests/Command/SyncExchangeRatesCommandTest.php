<?php
declare(strict_types=1);

namespace App\Tests\Command;

use App\Entity\ExchangeRate;
use App\Service\ExchangeRate\ExchangeRateFetcher;
use App\Tests\CommandTestCase;

class SyncExchangeRatesCommandTest extends CommandTestCase
{
    public function testSuccessfullUpdateExchangeRates()
    {
        $fetcherData = [
            ['name' => 'Euro', 'code' => 'EUR', 'buy_rate' => 4.30, 'sell_rate' => 4.50, 'date' => '2020-11-27'],
            ['name' => 'Dolar amerykański', 'code' => 'USD', 'buy_rate' => 3.30, 'sell_rate' => 3.50, 'date' => '2020-11-27'],
            ['name' => 'Funt brytyjski', 'code' => 'GBP', 'buy_rate' => 5.30, 'sell_rate' => 5.50, 'date' => '2020-11-27']
        ];
        $fetcherMock = $this->createMock(ExchangeRateFetcher::class);
        $fetcherMock->expects($this->any())->method('fetch')->willReturn($fetcherData);
        $this->getContainer()->set(ExchangeRateFetcher::class, $fetcherMock);

        $tester = $this->getCommandTester('app:exchange-rates:sync');
        $tester->execute([]);
        $output = $tester->getDisplay();

        $dt = new \DateTime('2020-11-27');
        $rateEur = $this->getEntityManager()->getRepository(ExchangeRate::class)->findOneBy(['code' => 'EUR', 'date' => $dt]);
        $rateUsd = $this->getEntityManager()->getRepository(ExchangeRate::class)->findOneBy(['code' => 'USD', 'date' => $dt]);
        $rateGbp = $this->getEntityManager()->getRepository(ExchangeRate::class)->findOneBy(['code' => 'GBP', 'date' => $dt]);

        $this->assertSame(4.30, $rateEur->getBuyRate());
        $this->assertSame(4.50, $rateEur->getSellRate());
        $this->assertSame(3.30, $rateUsd->getBuyRate());
        $this->assertSame(3.50, $rateUsd->getSellRate());
        $this->assertSame(5.30, $rateGbp->getBuyRate());
        $this->assertSame(5.50, $rateGbp->getSellRate());
        $this->assertTrue(strpos($output, 'Updated 3 rates exchanges') !== false);
    }
}