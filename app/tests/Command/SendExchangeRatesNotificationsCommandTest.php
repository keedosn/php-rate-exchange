<?php
declare(strict_types=1);

namespace App\Tests\Command;

use App\Entity\Client;
use App\Entity\ClientSubscription;
use App\Entity\Currency;
use App\Tests\CommandTestCase;

class SendExchangeRatesNotificationsCommandTest extends CommandTestCase
{
    public function testIgnoreUnactivatedSubscribers()
    {
        $tester = $this->getCommandTester('app:exchange-rates:send');
        $tester->execute(['-d' => '2020-11-27']);
        $output = $tester->getDisplay();

        $this->assertTrue(strpos($output, 'Sent 1 notification emails') !== false);
    }

    public function testIgnoreUsersWithoutSubscriptions()
    {
        $this->activateClient('client2');

        $tester = $this->getCommandTester('app:exchange-rates:send');
        $tester->execute(['-d' => '2020-11-27']);
        $output = $tester->getDisplay();

        $this->assertTrue(strpos($output, 'Sent 1 notification emails') !== false);
    }

    public function testShouldSendEmailToEachActiveUserWithSubscribtion()
    {
        $this->activateClient('client2');
        $this->addSecondUserSubscribtions();

        $tester = $this->getCommandTester('app:exchange-rates:send');
        $tester->execute(['-d' => '2020-11-27']);
        $output = $tester->getDisplay();

        $this->assertTrue(strpos($output, 'Sent 2 notification emails') !== false);
    }

    private function addSecondUserSubscribtions(): void
    {
        $em = $this->getEntityManager();
        $client2 = $em->getRepository(Client::class)->findOneByEmail('user2@example.org');

        $rates = [
            'EUR' => ['low' => 4.0, 'high' => 4.5],
            'USD' => ['low' => 3.0, 'high' => 3.5],
            'GBP' => ['low' => 5.0, 'high' => 5.5]
        ];
        foreach ($rates as $currencyCode => $limit) {
            $currency = $em->getRepository(Currency::class)->findOneByCode($currencyCode);
            $subscription = new ClientSubscription();
            $subscription->setClient($client2);
            $subscription->setCurrency($currency);
            $subscription->setLowRateAlert($limit['low']);
            $subscription->setHighRateAlert($limit['high']);

            $em->persist($subscription);
        }
        $em->flush();
    }
}