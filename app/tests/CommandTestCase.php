<?php
declare(strict_types=1);

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class CommandTestCase extends FixturesTestCase
{
    private Application $application;

    public function setUp(): void
    {
        parent::setUp();

        $this->application = new Application(static::$kernel);
    }

    protected function getCommandTester(string $commandName): CommandTester
    {
        $command = $this->application->find($commandName);

        return new CommandTester($command);
    }
}
