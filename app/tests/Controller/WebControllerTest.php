<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use App\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class WebControllerTest extends WebTestCase
{
    protected static string $defaultPath = '/confirm';

    protected string $defaultMethod = 'get';

    public function testConfirmRegistrationShouldReturnErrorWhenPassedTokenNotExists()
    {
        $response = $this->getWebResponse(static::$defaultPath . '/NotExistingToken');
        $content = $this->checkWebResponse($response, Response::HTTP_NOT_FOUND);
        
        $this->assertTrue(strpos($content, 'Client not found') !== false);
    }

    public function testConfirmRegistrationShouldReturnErrorWhenClientAlreadyConfirmed()
    {
        $client = $this->getFixtures()->getFixtureObject('client1');
        $response = $this->getWebResponse(static::$defaultPath . '/'  .$client->getConfirmationToken());
        $content = $this->checkWebResponse($response, Response::HTTP_BAD_REQUEST);
        
        $this->assertTrue(strpos($content, 'Client already confirmed') !== false);
    }

    public function testConfirmRegistrationShouldConfirmClientSuccessful()
    {
        $client = $this->getFixtures()->getFixtureObject('client2');
        $response = $this->getWebResponse(static::$defaultPath . '/'  .$client->getConfirmationToken());
        $content = $this->checkWebResponse($response, Response::HTTP_OK);

        $this->assertTrue(strpos($content, 'You\'re been successfuly registered in Exchange Rate Notification Service.') !== false);
    }
}