<?php
declare(strict_types=1);

namespace App\Tests\Controller\Api\ClientController;

use App\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class PostRegisterActionTest extends WebTestCase
{
    protected static string $defaultPath = '/api/client';

    protected string $defaultMethod = 'POST';

    public function testRegisterWithInvalidData()
    {
        $response = $this->getClientResponse(null, null, null, '{}');
        $content = $this->checkJsonResponse($response, Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertTrue(strpos($content->message, 'Object(App\Entity\Client).email:') !== false);
        $this->assertTrue(strpos($content->message, 'Object(App\Entity\Client).firstName:') !== false);
        $this->assertTrue(strpos($content->message, 'Object(App\Entity\Client).lastName:') !== false);
        $this->assertTrue(strpos($content->message, 'Object(App\Entity\Client).phoneNumber:') !== false);
        $this->assertTrue(strpos($content->message, 'Object(App\Entity\Client).dob:') !== false);
    }

    public function testRegisterWithTakenEmail()
    {
        $data = ['email' => 'user1@example.org'];
        $response = $this->getClientResponse(null, null, null, $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertTrue(strpos($content->message, 'This value is already used.') !== false);
    }

    public function testRegisterWithInvalidPhoneNumber()
    {
        $data = ['phoneNumber' => 12345];
        $response = $this->getClientResponse(null, null, null, $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertTrue(strpos($content->message, 'This value should have exactly 9 characters.') !== false);
    }

    public function testRegisterWithInvalidBirthDate()
    {
        $data = ['dob' => '2020-01-01'];
        $response = $this->getClientResponse(null, null, null, $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertTrue(strpos($content->message, 'Object(App\Entity\Client).dob') !== false);
        $this->assertTrue(strpos($content->message, 'This value should be less than') !== false);
    }

    public function testRegisterSuccessful()
    {
        $data = [
            'email' => 'new-user@example.org',
            'firstName' => 'FirstName',
            'lastName' => 'LastName',
            'phoneNumber' => 518456789,
            'dob' => '2002-01-01'
        ];
        $response = $this->getClientResponse(null, null, null, $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_OK);
        
        $this->assertSame('new-user@example.org', $content->email);
        $this->assertSame('FirstName', $content->firstName);
        $this->assertSame('LastName', $content->lastName);
        $this->assertSame(518456789, $content->phoneNumber);
        $this->assertSame('2002-01-01T00:00:00+00:00', $content->dob);
    }
}