<?php
declare(strict_types=1);

namespace App\Tests\Controller\Api\ClientSubscriptionController;

use App\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class PostSubscriptionsActionTest extends WebTestCase
{
    protected static string $defaultPath = '/api/subscription';

    protected string $defaultMethod = 'POST';

    public function testShouldReturnErrorWhenUnauthorized()
    {
        $response = $this->getClientResponse();
        $content = $this->checkJsonResponse($response, Response::HTTP_UNAUTHORIZED);

        $this->assertSame('Authentication Required', $content->message);
    }

    public function testShouldReturnErrorWhenInvalidCredentialsPassed()
    {
        $client = $this->getFixtures()->getFixtureObject('client2');
        $response = $this->getClientResponse(null, null, $client->getApiToken());
        $content = $this->checkJsonResponse($response, Response::HTTP_UNAUTHORIZED);

        $this->assertSame('Invalid credentials.', $content->message);
    }

    public function testShouldDoNothingWhenEmptySubscriptionListPassed()
    {
        $this->activateClient('client2');
        $client = $this->getFixtures()->getFixtureObject('client2');
        $response = $this->getClientResponse(null, null, $client->getApiToken(), []);
        $content = $this->checkJsonResponse($response);

        $this->assertcount(0, $content);
    }

    public function testShouldReturnErrorWhenInvalidSubscriptionDetailsPassed()
    {
        $data = [
            ['currencyCode' => 'ASD']
        ];
        $this->activateClient('client2');
        $client = $this->getFixtures()->getFixtureObject('client2');
        $response = $this->getClientResponse(null, null, $client->getApiToken(), $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertTrue(strpos($content->message, 'Object(App\Entity\ClientSubscription).lowRateAlert') !== false);
        $this->assertTrue(strpos($content->message, 'Object(App\Entity\ClientSubscription).highRateAlert') !== false);
    }

    public function testShouldReturnErrorWhenMissingCurrencyCodeParam()
    {
        $data = [
            ['lowRateAlert' => '4', 'highRateAlert' => '4.5'],
        ];
        $this->activateClient('client2');
        $client = $this->getFixtures()->getFixtureObject('client2');
        $response = $this->getClientResponse(null, null, $client->getApiToken(), $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertSame('Missing currencyCode param', $content->message);
    }

    public function testShouldSkipSubscriptionToNotExistingCurrency()
    {
        $data = [
            ['currencyCode' => 'ASD', 'lowRateAlert' => '4', 'highRateAlert' => '4.5'],
            ['currencyCode' => 'ZYX', 'lowRateAlert' => '4', 'highRateAlert' => '4.5']
        ];
        $this->activateClient('client2');
        $client = $this->getFixtures()->getFixtureObject('client2');
        $response = $this->getClientResponse(null, null, $client->getApiToken(), $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_OK);

        $this->assertCount(0, $content);
    }

    public function testCreateSubscriptionWith()
    {
        $data = [
            ['currencyCode' => 'EUR', 'lowRateAlert' => '4', 'highRateAlert' => '4.5'],
            ['currencyCode' => 'USD', 'lowRateAlert' => '3', 'highRateAlert' => '3.5'],
            ['currencyCode' => 'GBP', 'lowRateAlert' => '5', 'highRateAlert' => '5.5']
        ];
        $this->activateClient('client2');
        $client = $this->getFixtures()->getFixtureObject('client2');
        $response = $this->getClientResponse(null, null, $client->getApiToken(), $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_OK);

        $this->assertCount(3, $content);
    }
}