<?php
declare(strict_types=1);

namespace App\Tests\Controller\Api\ClientSubscriptionController;

use App\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class GetSubscriptionsActionTest extends WebTestCase
{
    protected static string $defaultPath = '/api/subscription';

    protected string $defaultMethod = 'GET';

    public function testShouldReturnErrorWhenUnauthorized()
    {
        $response = $this->getClientResponse();
        $content = $this->checkJsonResponse($response, Response::HTTP_UNAUTHORIZED);

        $this->assertSame('Authentication Required', $content->message);
    }

    public function testShouldReturnErrorWhenInvalidCredentialsPassed()
    {
        $client = $this->getFixtures()->getFixtureObject('client2');
        $response = $this->getClientResponse(null, null, $client->getApiToken());
        $content = $this->checkJsonResponse($response, Response::HTTP_UNAUTHORIZED);

        $this->assertSame('Invalid credentials.', $content->message);
    }

    public function testShouldReturnEmptyListWhenUserHasNoSubscriptions()
    {
        $this->activateClient('client2');
        $client = $this->getFixtures()->getFixtureObject('client2');
        $response = $this->getClientResponse(null, null, $client->getApiToken());
        $content = $this->checkJsonResponse($response, Response::HTTP_OK);

        $this->assertCount(0, $content);
    }

    public function testShouldReturnUserSubscriptions()
    {
        $client = $this->getFixtures()->getFixtureObject('client1');
        $response = $this->getClientResponse(null, null, $client->getApiToken());
        $content = $this->checkJsonResponse($response, Response::HTTP_OK);
        
        $this->assertCount(3, $content);
    }
}
