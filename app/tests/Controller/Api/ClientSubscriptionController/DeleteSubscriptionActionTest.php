<?php
declare(strict_types=1);

namespace App\Tests\Controller\Api\ClientController;

use App\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class DeleteSubscriptionActionTest extends WebTestCase
{
    protected static string $defaultPath = '/api/subscription';

    protected string $defaultMethod = 'DELETE';

    public function testShouldReturnErrorWhenUnauthorized()
    {
        $response = $this->getClientResponse();
        $content = $this->checkJsonResponse($response, Response::HTTP_UNAUTHORIZED);

        $this->assertSame('Authentication Required', $content->message);
    }

    public function testShouldReturnErrorWhenInvalidCredentialsPassed()
    {
        $client = $this->getFixtures()->getFixtureObject('client2');
        $response = $this->getClientResponse(null, null, $client->getApiToken());
        $content = $this->checkJsonResponse($response, Response::HTTP_UNAUTHORIZED);

        $this->assertSame('Invalid credentials.', $content->message);
    }

    public function testShouldReturnErrorWhenNotExistingCurrencyPassed()
    {
        $client = $this->getFixtures()->getFixtureObject('client1');
        $data = ['currencyCode' => 'ASD', 'unsubscribeToken' => '1EAAB030-6C86-4A36-BC52-DBEBD86E01EF'];
        $response = $this->getClientResponse(null, null, $client->getApiToken(), $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_NOT_FOUND);

        $this->assertSame('Currency not exists', $content->message);
    }

    public function testShouldReturnErrorWhenUnsubscribeTokenIsInvalid()
    {
        $client = $this->getFixtures()->getFixtureObject('client1');
        $data = ['currencyCode' => 'GBP', 'unsubscribeToken' => 'NOT_UUID_VALUE'];
        $response = $this->getClientResponse(null, null, $client->getApiToken(), $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertTrue(strpos($content->message, 'This is not a valid UUID') !== false);
    }

    public function testShouldReturnErrorWhenSubscriptionNotExists()
    {
        $subscription = $this->getFixtures()->getFixtureObject('client1_gbp');
        $client = $this->getFixtures()->getFixtureObject('client1');

        $data = ['currencyCode' => 'ASD', 'unsubscribeToken' => $subscription->getUnsubscribeToken()];
        $response = $this->getClientResponse(null, null, $client->getApiToken(), $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_NOT_FOUND);

        $this->assertSame('Currency not exists', $content->message);
    }

    public function testShouldSuccessfullRemoveSubscription()
    {
        $subscription = $this->getFixtures()->getFixtureObject('client1_gbp');
        $client = $this->getFixtures()->getFixtureObject('client1');

        $data = ['currencyCode' => 'GBP', 'unsubscribeToken' => $subscription->getUnsubscribeToken()];
        $response = $this->getClientResponse(null, null, $client->getApiToken(), $data);
        $content = $this->checkJsonResponse($response, Response::HTTP_NO_CONTENT);
    }
}