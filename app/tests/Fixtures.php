<?php

declare(strict_types=1);

namespace App\Tests;

class Fixtures
{
    private static Fixtures $instance;
    private array $fixtures;

    public static function createFixturesInstance(array $fixtures): Fixtures
    {
        if (isset(self::$instance)) {
            throw new \LogicException('Fixtures can be loaded once.');
        }
        $instance = new Fixtures();
        $instance->setFixtures($fixtures);
        self::$instance = $instance;

        return self::$instance;
    }

    public static function getFixturesInstance(): Fixtures
    {
        if (!isset(self::$instance)) {
            throw new \LogicException('Fixtures instance in not created');
        }

        return self::$instance;
    }

    public function getFixtures(): array
    {
        return $this->fixtures;
    }

    public function getFixtureObject(string $fixtureId): ?object
    {
        if (array_key_exists($fixtureId, $this->getFixtures())) {
            return ($this->getFixtures())[$fixtureId];
        }

        return null;
    }

    private function setFixtures(array $fixtures)
    {
        $this->fixtures = $fixtures;
    }
}
